type _extendableEvent('a);
type extendableEvent_like('a) = Dom.event_like(_extendableEvent('a));
type t = extendableEvent_like(Dom._baseClass);

[@bs.send] external waitUntil: (t, Js.Promise.t('a)) => unit
  = "waitUntil";


