type dir = [
  | `auto
  | `ltr
  | `rtl
]

type t('data) = {
  dir: dir,
  lang: string,
  badge: string,
  body: string,
  tag: string,
  icon: string,
  image: string,
  data: 'data,
  vibrate: list(int),
  renotify: bool,
  requireInteraction: bool,
  actions: list(NotificationAction.t),
  silent: bool
}